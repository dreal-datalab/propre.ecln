#' graphique sur un territoire sur un indicateur en moyenne annuelle et valeur trimestrielle
#'
#' @param data Le dataframe avec les données ecln.
#' @param indicateur  L'indicateur à visualiser.
#' @param zone Le territoire sur lequel visualiser l'indicateur.
#' @param titre Le titre du graphique.
#'
#' @return  un ggplot2
#' @export
#' @importFrom dplyr select distinct pull filter mutate
#' @importFrom forcats fct_recode
#' @importFrom ggforce geom_mark_circle
#' @importFrom ggplot2 ggplot aes geom_line geom_point margin theme labs guides scale_y_continuous scale_x_date
#' @importFrom glue glue
#' @importFrom gouvdown gouv_colors scale_color_gouv_discrete scale_fill_gouv_discrete
#' @importFrom grid unit
#' @importFrom lubridate quarter year
#' @importFrom stringr str_c
creer_graphique_evolution_trim <- function(data = indic_ecln,
                                           indicateur = "Ventes - Logements",
                                           zone = "Pays de la Loire",
                                           titre = "Ventes de logements neufs en Pays de la Loire") {
  per <- data %>%
    dplyr::select(.data$Periode) %>%
    dplyr::distinct() %>%
    dplyr::pull(.data$Periode)

  max_date <- max(per)

  sous_titre <- glue::glue("Au {label_rang(lubridate::quarter(max_date))} trimestre {lubridate::year(max_date)}")
  label <- data %>%
    dplyr::filter(
      .data$Zone == zone,
      .data$Indicateur == indicateur
    ) %>%
    dplyr::filter(.data$Periode == max(.data$Periode)) %>%
    dplyr::mutate(
      Valeur = ifelse(.data$TypeIndicateur == "Cumul annuel", .data$Valeur / 4, .data$Valeur),
      TypeIndicateur = forcats::fct_recode(.data$TypeIndicateur,
        "Moyenne annuelle" = "Cumul annuel",
        "Valeur trimestrielle" = "Trimestriel"
      )
    ) %>%
    dplyr::mutate(
      description = stringr::str_c(
        format(round(.data$Valeur), big.mark = " "),
        "\n",
        ifelse(.data$TauxEvolution12Mois > 0, "+", ""),
        format(round(.data$TauxEvolution12Mois, 1), big.mark = " ", decimal.mark = ","),
        " %"
      ),
      label = ifelse(.data$TypeIndicateur %in% c("Valeur trimestrielle"),
        FormatCaractere(.data$Periode),
        "Moyenne\nannuelle"
      )
    )

  gg <- data %>%
    dplyr::filter(
      .data$Zone == zone,
      .data$Indicateur == indicateur
    ) %>%
    dplyr::mutate(
      Valeur = ifelse(.data$TypeIndicateur == "Cumul annuel", .data$Valeur / 4, .data$Valeur),
      TypeIndicateur = forcats::fct_recode(.data$TypeIndicateur,
        "Moyenne annuelle" = "Cumul annuel",
        "Valeur trimestrielle" = "Trimestriel"
      )
    ) %>%
    ggplot2::ggplot() +
    ggplot2::aes(x = .data$Periode, y = .data$Valeur, group = .data$TypeIndicateur, linetype = .data$TypeIndicateur, color = .data$TypeIndicateur) +
    ggplot2::geom_line() +
    ggplot2::geom_point(data = label, ggplot2::aes(x = .data$Periode, y = .data$Valeur, group = .data$TypeIndicateur, color = .data$TypeIndicateur)) +
    ggforce::geom_mark_circle(
      data = label, ggplot2::aes(
        label = .data$label,
        fill = .data$TypeIndicateur,
        description = .data$description
      ),
      label.buffer = grid::unit(5, "mm"),
      expand = grid::unit(2, "mm"),
      label.margin = ggplot2::margin(2, 5, 2, 5, "mm"),
      label.colour = "white",
      alpha = .5,
      label.fill = gouvdown::gouv_colors("bleu_france")
    ) +
    ggplot2::theme(legend.position = "bottom") +
    gouvdown::scale_color_gouv_discrete(palette = "pal_gouv_qual2") +
    gouvdown::scale_fill_gouv_discrete(palette = "pal_gouv_qual2") +
    ggplot2::labs(
      x = "",
      y = "Nombre de ventes",
      title = titre,
      subtitle = sous_titre,
      caption = "Source : ECLN\nMoyenne Annuelle : Valeur observ\u00e9e en moyenne sur les 4 derniers trimestres"
    ) +
    ggplot2::guides(fill = F) +
    ggplot2::scale_y_continuous(labels = function(l) format(l, scientific = FALSE, big.mark = " ", decimal.mark = ","), limits = c(0, NA)) +
    ggplot2::scale_x_date(date_breaks = "1 year", date_labels = "%Y", limits = c(min(per), max(per) + months(12)))

  return(gg)
}
