% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/data_prep.R
\name{data_prep}
\alias{data_prep}
\title{datapreparation des données}
\usage{
data_prep(reg, abc = TRUE, zone_a_secretiser = "", marches_a_secretiser = "")
}
\arguments{
\item{reg}{code region}

\item{abc}{TRUE si on veut garder les zonages abc}

\item{zone_a_secretiser}{listes des zonages à secretiser}

\item{marches_a_secretiser}{liste des marchés à secretiser : individuel, collectif, tous}
}
\value{
un dataframe filtré sur la région souhaitée
}
\description{
datapreparation des données
}
